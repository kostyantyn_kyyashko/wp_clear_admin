<?php
/*
* Created by Kostyantyn Kyyashko
* k.konstantin.n@gmail.com
*/
function plugin_activate()
{
    create_clear_db_table();
    fill_data_to_db();
}

function plugin_deactivate()
{
    drop_db_table();
    delete_option('hide_admin_bar');
    delete_option('hide_admin_bar_front');
    delete_option('selected_item_color');
}

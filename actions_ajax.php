<?php
add_action( 'wp_ajax_change_status', 'change_status' );
add_action( 'wp_ajax_change_order', 'change_order' );
add_action('wp_ajax_change_icon_color', 'change_icon_color');
add_action('wp_ajax_save_icon', 'save_icon');
add_action('wp_ajax_hide_admin_setup', 'hide_admin_setup');
add_action('wp_ajax_hide_admin_setup_front', 'hide_admin_setup_front');
add_action('wp_ajax_change_selected_item_color', 'change_selected_item_color');
add_action('wp_ajax_change_icon_color', 'change_icon_color');
add_action('wp_ajax_file_uploader', 'file_uploader');





<?php
add_action('admin_enqueue_scripts', 'add_scripts_to_admin');
add_action('admin_enqueue_styles', 'add_favicon');
add_action('wp_enqueue_scripts', 'add_scripts_to_front');
add_action('admin_menu', 'add_admin_menu_page_to_menu_and_db');
add_action('admin_menu', 'menu_items_after_clear');
add_action('admin_enqueue_scripts', 'menu_items_color');
add_action( 'wp_dashboard_setup', 'clear_wp_dash' );
remove_action( 'welcome_panel', 'wp_welcome_panel' );
add_action('in_admin_header', 'zero_panel');
add_action('admin_enqueue_scripts', 'hide_admin_bar_action');


add_action('wp', 'hide_admin_bar_front_action');
add_action('wp_head', 'add_favicon');
add_action('admin_head', 'add_favicon');
add_action( 'login_head', 'add_favicon' );
add_action( 'login_enqueue_scripts', 'ul_logo' );
add_filter( 'login_headerurl', 'custom_login_logo_url' );
add_filter( 'login_headertext', 'custom_login_logo_url_title' );

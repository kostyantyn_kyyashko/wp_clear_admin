<?php

function menu_items_control_table()
{
    global $wpdb;
    unset($menu);
    $table_header = <<<html
    <div class="table-responsive-sm" id="control_admin_menu_items">
    <h5>&#9660; Control Admin Menu Items</h5> 
    <table class="table table-striped table-hover table-responsive-sm table-bordered" id="admin_menu_items">
        <thead>
        <tr>
            <th data-tooltip 
                title="The menu items are arranged in the order defined by these numbers, in ascending order" 
                style="width: 1%">
                Order
            </th>
            <th data-tooltip 
                title="Name of dashboard menu. Not can be edited"
                style="width: 3%;">
                Menu Item
            </th>
            <th style="width: 3%;">User can</th>
            <th style="width: 3%;" title="relative URL or callable funcion">Handler<span style="color: blue; font-size: 130%">*</span> </th>
            <th style="width: 3%;">Icon View</th>
            <th style="width: 1%">Status</th>
        </tr>
        </thead>
        <tbody>
html;
    $sql2 = <<<sql
SELECT * from `ul_clear_admin` 
sql;

    $out = json_decode(json_encode($wpdb->get_results($sql2)), 1);
    $table = '';
    $table_rows = '';
    usort($out, function($a, $b) {
        if ($a['order'] > $b['order']) {
            return 1;
        }
        return -1;
   });
    foreach ($out as $key=>$item):
        if(strstr($item['handler'], 'separator')) continue;
        ob_start();
        ?>
        <tr>
            <td><input class="order" value="<?=$item['order']?>" style="width: 30px;"></td>
            <td><b><?=$points[]=$item['menu_title']?></b></td>
            <td><?=$item['role']?></td>
            <? $handler = strstr($item['handler'], '.php')?$item['handler']:"admin.php?page={$item['handler']}" ?>
            <td class="handler"><a href="/wp-admin/<?=$handler?>"><?=$item['handler']?></a></td>
            <td class="align-middle td-icon" style="text-align: center;
                background-color: #23282D;">
                <div class="dashicons <?=$item['icon_css']?>"></div>
            </td>
            <td class="align-middle status" style="text-align: center;"><? if($item['status']=='true'): ?><span class="dashicons dashicons-admin-post"></span> <?endif;?></td>
        </tr>
        <?
        $table_rows .= ob_get_contents();
        ob_end_clean();
    endforeach;
    $table .= $table_header .  $table_rows . '</tbody></table></div>';
    $table = "<div style = font-size:80%>{$table}</div>";
    echo $table . '<hr class="separator">';
}

function change_admin_bar_display()
{
    $hide_in_admin = !get_option('hide_admin_bar')?'Now Hides. Show It!':'Now Shows. Hide It!';
    $hide_in_front = !get_option('hide_admin_bar_front')?'Now Hides. Show It!':'Now Shows. Hide It!';
    $button_class_admin = ($hide_in_admin == 'Now Shows. Hide It!')?'btn-info':'btn-warning';
    $button_class_front = ($hide_in_front == 'Now Shows. Hide It!')?'btn-info':'btn-warning';
    $table = <<<html
    <div class="table-responsive-sm" id="control_admin_menu_items">
    <h5>&#9660; Control Top ToolBar</h5> 
    <table class="table table-striped table-hover table-responsive-sm table-bordered" style="width: 600px;">
        <tbody>
        <tr>
            <td style="text-align: left;">Top admin toolbar in the dashboard</td>
            <td style="text-align: right;">Top admin toolbar in the site</td>
        </tr>
        <tr>
            <td style="width: 300px;">
                <button class="btn {$button_class_admin} btn-sm" id="hide_show_admin">{$hide_in_admin}</button>
            </td>
            <td style="width: 300px; text-align: right;">
                <button class="btn {$button_class_front} btn-sm" id="hide_show_admin_front">{$hide_in_front}t</button>
            </td>
        </tr>
        </tbody>
    </table>
</div>
html;
    echo $table;
}

function change_select_menu_item_color()
{ ?>
    <h5 class="color_select">&#9660; Choose Selected Item Color
        <select style="width: 150px !important;" class="form-control" id="choose_selected_item_color">
            <option selected disabled value="#" style="visibility: hidden"></option>
            <? $icon_colors = all_colors();
            foreach ($icon_colors as $color_name => $hex_value):?>
                <option class="form-control" value="<?=$color_name?>" style="background-color: <?=$hex_value?>"><?=$color_name?></option>
            <? endforeach;?>
        </select>
    </h5>
<?}

function uploader_in_admin_panel()
{
    ?>
    <h5 class="color_select">&#9660; Choose and change your logo</h5>
    <div>
        <form id="file_uploader" method="post" >
            <input type="text" name="action" value="file_uploader" hidden="hidden">
            <input type="file" name="logo" />
        </form>
    </div>
    <div id="logo_table">
        <table class="table table-striped" style="width: auto;">
            <tbody>
            <tr>
                <td>Logo in the login page</td>
                <td>Favicon</td>
            </tr>
            <tr>
                <?
                $ext = '';
                if(file_exists(plugin_dir_path(__FILE__) . 'img/logo64.gif')) {
                    $ext = 'gif';
                }
                if(file_exists(plugin_dir_path(__FILE__) . 'img/logo64.jpeg'))  {
                    $ext = 'jpeg';
                }
                if(file_exists(plugin_dir_path(__FILE__) . 'img/logo64.png')) {
                    $ext = 'png';
                }
                if ($ext) {
                    ?>
                    <td><img src="<?= plugin_dir_url(__FILE__) ?>img/logo64.<?= $ext ?>"></td>
                    <td><img src="<?= plugin_dir_url(__FILE__) ?>img/logo32.<?= $ext ?>"></td>
                    <?
                }
                else {
                    ?>
                    <td><img src="<?= plugin_dir_url(__FILE__) ?>img_original/logowp.png"></td>
                    <td><img src="<?= plugin_dir_url(__FILE__) ?>img_original/logowp32.png"></td>
                    <?
                }
                ?>
            </tr>
            </tbody>
        </table>
    </div>
    <?
}

function all_colors()
{
    $out = [];
    $out['black'] = '#000000';
    $out['white'] = '#FFFFFF';
    $out['red'] = '#FF0000';
    $out['lime'] = '#00FF00';
    $out['blue'] = '#0000FF';
    $out['yellow'] = '#FFFF00';
    $out['cyan'] = '#00FFFF';
    $out['magenta'] = '#FF00FF';
    $out['silver'] = '#C0C0C0';
    $out['gray'] = '#808080';
    $out['maroon'] = '#800000';
    $out['olive'] = '#808000';
    $out['green'] = '#008000';
    $out['purple'] = '#800080';
    $out['teal'] = '#008080';
    $out['navy'] = '#000080';

    return $out;
}

function create_clear_db_table()
{
    global $wpdb;
    $sql = <<<sql
CREATE TABLE IF NOT EXISTS `ul_clear_admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_title` varchar(255) DEFAULT NULL,
  `role` varchar(255) DEFAULT NULL,
  `handler` varchar(255) DEFAULT NULL,
  `page_title` varchar(255) DEFAULT NULL,
  `menu_css` varchar(255) DEFAULT NULL,
  `css_class` text DEFAULT NULL,
  `icon_css` varchar(255) DEFAULT NULL,
  `order` int(11) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `color` varchar (20) DEFAULT NULL,
  `icon_color` varchar (20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX UK_ul_clear_admin (`handler`)
)
ENGINE = INNODB;
sql;
    $wpdb->query($sql);
}


function add_admin_menu_page_to_menu_and_db()
{
    add_menu_page('Clear Admin', 'Clear Admin', 'read',
        'clear_admin','assembler', 'dashicons-list-view', 10);
    global $wpdb;
    $menu_css = <<<css
menu-top menu-icon-post open-if-no-js menu-top-first
css;
    $css_class = "menu-appearance2";
    $icon_css = "dashicons-list-view";

    $sql = <<<sql
INSERT IGNORE INTO `ul_clear_admin`
(`menu_title`, `role`, `handler`, `page_title`, `menu_css`, `css_class`, `icon_css`, `order`, `status`)
VALUES 
('Clear Admin','read','clear_admin','Clear Admin','{$menu_css}', '{$css_class}', '{$icon_css}', 55,'true')        
sql;
    $wpdb->query($sql);
}


function fill_data_to_db()
{
    global $menu;
    global $wpdb;
    foreach ($menu as $key=>$out) {
        $value5 = isset($out[5])?$out[5]:'';
        $value6 = isset($out[6])?$out[6]:'';
        $sql = <<<sql
INSERT IGNORE INTO `ul_clear_admin`
(`menu_title`, `role`, `handler`, `page_title`, `menu_css`, `css_class`, `icon_css`, `order`, `status`, `color`)
VALUES 
('{$out[0]}','{$out[1]}','{$out[2]}','{$out[3]}','{$out[4]}','{$value5}','{$value6}',{$key},'true', null)

sql;
        $wpdb->query($sql);
    }
}

function drop_db_table()
{
    global $wpdb;
    $sql = <<<sql
DROP TABLE IF EXISTS `ul_clear_admin`; 
sql;
    $wpdb->query($sql);
}

function ext()
{
    if (file_exists(plugin_dir_path(__FILE__) . 'img/ext.txt')) {
        return file_get_contents(plugin_dir_url(__FILE__) . 'img/ext.txt');
    }
    return null;
}

function add_scripts_to_admin() {
    global $icons_style;
    wp_enqueue_style('bootstrap_css', plugin_dir_url(__FILE__) . 'css/bootstrap.css');
    wp_enqueue_style('style1', plugin_dir_url(__FILE__) . 'css/clear.css');
    ?><link rel="stylesheet" href="<?=plugin_dir_url(__FILE__) . 'css/tooltip.css'?>"> <?
    if ($ext = ext()):
    ?><link rel="shortcut icon" href="<?=plugin_dir_url(__FILE__)?>img/logo32.<?=$ext;?>"/><?
        endif;?><?
    ?><script src="<?=plugin_dir_url(__FILE__)?>js/jquery.js'"></script><?
    ?><script src="<?=plugin_dir_url(__FILE__)?>js/bootstrap.js'"></script><?
    ?><script src="<?=plugin_dir_url(__FILE__)?>js/popper.js'"></script><?
    ?><script src="<?=plugin_dir_url(__FILE__)?>js/jquery.cookie.js'"></script><?
    ?><script src="<?=plugin_dir_url(__FILE__)?>js/tooltip.js'"></script><?
    ?><script src="<?=plugin_dir_url(__FILE__)?>js/ul.js'"></script><?
    if(in_array('clear_admin', $_GET)) {
        wp_enqueue_style('custom_for_table', plugin_dir_url(__FILE__) . 'css/custom_for_table.css');
    }
    ?>
    <style><?=$icons_style?></style>
    <div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-body">
                <? require plugin_dir_path(__FILE__) . 'icons.php'; echo $admin_menu_icons?>
              </div>
            </div>
        </div>
    </div>

<?
}

function add_scripts_to_front()
{
    if ($ext = ext()) {
        wp_enqueue_style('style2', plugin_dir_url(__FILE__) . 'css/clear_front.css');
        ?>
        <link rel="shortcut icon" href="<?=$ext;?>"/>
        <?
    }
    return;
}


global $icons_style;
function menu_items_after_clear()
{
    global $menu;
    global $wpdb;
    global $icons_style;
    $sql2 = <<<sql
SELECT * from `ul_clear_admin` where `status` = 'true'  
sql;

    $out = json_decode(json_encode($wpdb->get_results($sql2)), 1);

    usort($out, function($a, $b) {
        if ($a['order'] > $b['order']) {
            return 1;
        }
        return -1;
    });
    foreach ($out as $o) {
        $sub_out[$o['order']] = $o;
    }
    $d = [];
    $icons_style = '';
    foreach ($sub_out as $order => $inner_array){
        unset($inner_array['id']);
        if ($inner_array['icon_color']) {
            $icons_style .= <<<css
.{$inner_array['icon_css']}, div.{$inner_array['icon_css']}:before { color: {$inner_array['icon_color']} !important; }
css;
        }
        $d[$order] = array_values($inner_array);
    }
    $menu = $d;
}

function clear_wp_dash()
{
    unset($GLOBALS['wp_meta_boxes']);
}

function zero_panel()
{
    if ($_SERVER['REQUEST_URI'] == '/wp-admin/'){
        if (is_callable('admin_main_page_callable')) {
            admin_main_page_callable();
        }
    }
}

function hide_admin_bar_action()
{
    if (!get_option('hide_admin_bar') && is_admin()) {
        ?>
        <style>
            #wpadminbar {display: none; margin-top: -40px;}
            #wpwrap {margin-top: -40px;}
        </style>
        <?
    }
}

function hide_admin_bar_front_action()
{
    if (!get_option('hide_admin_bar_front') && !is_admin()) {
        ?>
        <style>
            #wpadminbar {display: none; margin-top: -40px;}
            #wpwrap {margin-top: -40px;}
        </style>
        <?
    }
}



function menu_items_color()
{
    $background_color = get_option('selected_item_color');
    $css = "";
    if ($background_color) {
        $css = "
        <style>
            li.current div, .wp-has-current-submenu, #choose_selected_item_color {
                background-color: {$background_color} !important;
            }
        </style>
";
    }
    echo $css;
}

function ul_logo()
{
    if ($ext = ext()) {

            ?>
            <style>
                body.login h1 a {
                    background: url(<?=plugin_dir_url(__FILE__) . 'img/logo64.' . $ext;?>) no-repeat;
                }
            </style>
            <?
        }
}

function custom_login_logo_url()
{
    return get_site_url();
}

function custom_login_logo_url_title() {
    return get_bloginfo('name');
}

function add_favicon() {
    if ($ext = ext()) {
        ob_start();
        ?>
        <link rel="shortcut icon" href="<?=plugin_dir_url(__FILE__)?>img/logo32.<?=$ext;?>"/>
        <?
        $style = ob_get_contents();
        ob_end_clean();
        echo  $style;
    }
    return null;
}

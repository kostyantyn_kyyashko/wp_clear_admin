<?php
/**
 * @package UL
 * @version 1.0.1
 * Plugin Name: UL Clear Admin
*/
require_once 'functions.php';
require_once 'functions_ajax.php';
require_once 'actions.php';
require_once 'actions_ajax.php';
require_once 'plugin_activate_deactivate.php';
register_activation_hook(__FILE__, 'plugin_activate');
register_deactivation_hook(__FILE__, 'plugin_deactivate');

function assembler()
{
    menu_items_control_table();
    change_admin_bar_display();
    change_select_menu_item_color();
    uploader_in_admin_panel();
}

function admin_main_page_callable()
{

}


